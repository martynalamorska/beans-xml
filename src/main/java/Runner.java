import org.springframework.context.support.ClassPathXmlApplicationContext;
import pl.sda.teachers.EnglishTeacher;
import pl.sda.teachers.MathsTeacher;
import pl.sda.teachers.PhysicsTeacher;

public class Runner {
    public static void main(String[] args) {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        MathsTeacher mTeacher = context.getBean("mathsTeacher", MathsTeacher.class);
        MathsTeacher mTeacher2 = context.getBean("mathsTeacher", MathsTeacher.class);
        EnglishTeacher eTeacher = context.getBean("englishTeacher", EnglishTeacher.class);
        PhysicsTeacher pTeacher = context.getBean("physicsTeacher", PhysicsTeacher.class);
        System.out.println(mTeacher.greet());
        System.out.println(eTeacher.greet());
        System.out.println(pTeacher.greet());
        System.out.println(pTeacher.sayMotto());
        System.out.println(mTeacher.sayMotto());
        eTeacher.setEmailAddress("asdadwqe");
        System.out.println(eTeacher.getEmailAddress());
        System.out.println(mTeacher.getEmailAddress());

        if (mTeacher == mTeacher2) {
            System.out.println("the same");
        } else {
            System.out.println("different");
        }
    }
}
