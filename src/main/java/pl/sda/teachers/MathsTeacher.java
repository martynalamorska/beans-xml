package pl.sda.teachers;

import pl.sda.motto.SadMottoService;

public class MathsTeacher implements Teacher {

    private SadMottoService sadMottoService;

    public MathsTeacher setSadMottoService(SadMottoService sadMottoService) {
        this.sadMottoService = sadMottoService;
        return this;
    }

    public MathsTeacher() {
        System.out.println("ffgb");
    }

    public String greet(){
        return "Hi, I'm the Maths teacher.";
    }

    public String sayMotto() {
        return sadMottoService.getMotto();
    }

    public String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public MathsTeacher setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }
}
