package pl.sda.teachers;

public class EnglishTeacher implements Teacher {

    public String greet(){
        return "Hi, I'm the English teacher.";
    }

    public String sayMotto() {
        return null;
    }

    public String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public EnglishTeacher setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }
}
