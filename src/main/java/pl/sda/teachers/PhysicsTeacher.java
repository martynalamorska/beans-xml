package pl.sda.teachers;

import pl.sda.motto.HappyMottoService;

public class PhysicsTeacher implements Teacher {

    private final HappyMottoService happyMottoService;

    public String greet() {
        return "Hi, I'm the Physics teacher.";
    }

    public String sayMotto(){
        return happyMottoService.getMotto();
    }

    public PhysicsTeacher(HappyMottoService happyMottoService){
        this.happyMottoService = happyMottoService;
    }

    public String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public PhysicsTeacher setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }
}
